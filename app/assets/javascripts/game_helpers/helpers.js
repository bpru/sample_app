// frame class
function Frame() {
	this.draw_handler;

	this.set_draw_handler = function(draw) {
		this.draw_handler = draw_handler;
		function draw_handler() {
			draw();
			requestAnimationFrame(draw_handler);
		}
		
	};
	
	this.start = function(){
		if (this.draw_handler) {
			requestAnimationFrame(this.draw_handler);
		};
	};
};

// create frame
function create_frame() {return new Frame();};