// timer class
function Timer(func, millisec) {
	this.func = func;
	this.millisec = millisec;
	this.timer;
	this.is_running = false;
	this.start = function() {
		this.timer = setInterval(this.func, this.millisec);
		this.is_running = true;
	}
	this.stop = function() {
		clearInterval(this.timer);
		this.is_running = false;
	};
};

// create timer
function create_timer(func, millisec) {
	return new Timer(func, millisec);
};