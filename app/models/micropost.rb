class Micropost < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  
  mount_uploader :picture, PictureUploader
  # this method requires CarrierWave gem to create a uploader, it takes 
  # as arguments a symbol representing the attribute and the class name 
  # of the generated uploader
  
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  
  # use validate instead of validates for costume validation methods
  validate  :picture_size 
  
  private

    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
    
end
